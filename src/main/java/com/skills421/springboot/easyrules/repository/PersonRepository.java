package com.skills421.springboot.easyrules.repository;

import com.skills421.springboot.easyrules.model.Person;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface PersonRepository extends MongoRepository<Person, Long> {
    @Query("{name:'?0'}")
    Person findItemByLastName(String lastName);

}
