package com.skills421.springboot.easyrules.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Data
@ToString
@Document("person")
public class Person {
    @Id
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String mobile;
    private String phone;

}
