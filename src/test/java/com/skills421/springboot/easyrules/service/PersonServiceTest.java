package com.skills421.springboot.easyrules.service;

import com.skills421.springboot.easyrules.model.Person;
import com.skills421.springboot.easyrules.repository.PersonRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class PersonServiceTest {

    @Mock
    private PersonRepository personRepository;

    private PersonService personService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        personService = new PersonService(personRepository);
    }

    @Test
    public void testGetAllPersons() {
        List<Person> persons = new ArrayList<>();
        persons.add(createMockPerson("Jon", "Doe", "jon.doe@gmail.com", "07717 111222", "020 111 1234"));
        persons.add(createMockPerson("Jane", "Doe", "jane.doe@gmail.com", "07717 333444", "020 222 3456"));

        when(personRepository.findAll()).thenReturn(persons);

        List<Person> result = personService.getAllPersons();

        assertEquals(2, result.size());
        assertEquals("Jon", result.get(0).getFirstName());
        assertEquals("Doe", result.get(0).getLastName());
        // ... similar assertions for other properties
    }

    // Create similar test methods for other service methods

    private Person createMockPerson(String firstName, String lastName, String email, String mobile, String phone) {
        Person person = new Person();
        person.setFirstName(firstName);
        person.setLastName(lastName);
        person.setEmail(email);
        person.setMobile(mobile);
        person.setPhone(phone);
        return person;
    }
}



