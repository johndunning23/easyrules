package com.skills421.springboot.easyrules.controller;

import com.skills421.springboot.easyrules.model.Person;
import com.skills421.springboot.easyrules.service.PersonService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.Optional;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

public class PersonControllerTest {

    @Mock
    private PersonService personService;

    private PersonController personController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        personController = new PersonController(personService);
    }

    @Test
    public void testGetPersonById() {
        Long id = 1L;
        Person mockPerson = createMockPerson("Jon", "Doe", "jon.doe@gmail.com", "07717 111222", "020 111 1234");

        when(personService.getPersonById(id)).thenReturn(Optional.of(mockPerson));

        Optional<Person> result = personController.getPersonById(id);

        assertTrue(result.isPresent());
        assertEquals("Jon", result.get().getFirstName());
        assertEquals("Doe", result.get().getLastName());
        // ... similar assertions for other properties
    }

    // Create similar test methods for other controller methods

    private Person createMockPerson(String firstName, String lastName, String email, String mobile, String phone) {
        Person person = new Person();
        person.setFirstName(firstName);
        person.setLastName(lastName);
        person.setEmail(email);
        person.setMobile(mobile);
        person.setPhone(phone);
        return person;
    }
}



