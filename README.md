# Adding Swagger:

Spring Boot 3 needs a different library than Spring Boot 2 to be able to use Swagger 3

## Add the following maven dependency

```
<dependency>
   <groupId>org.springdoc</groupId>
   <artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
   <version>2.0.2</version>
</dependency>
```

### Check the dependency tree

mvn dependency:tree

Double check that `io.swagger.core.v3:swagger-core-jakarta` is there

### Context Path

Setting a context path in `application.properties` or `application.yml`:

```
server:
servlet:
context-path: /api/something
```

### Generic path to Swagger UI

http://server:port/context-path/swagger-ui.html
* http://localhost:8080/swagger-ui/index.html

### Run the IDE:

mvn spring-boot:run

### Swagger URI

http://localhost:8080/swagger-ui/index.html

